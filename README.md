ics-ans-role-nx-terminalsrv
===========================

Ansible role to install nx-terminalsrv.
This role install the nomachine terminal server Enterrise version

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
nx_terminalsrv_rpm: http://artifactory.esss.lu.se/artifactory/swi-pkg/nomachine/nomachine-terminal-server_6.4.6_1_x86_64.rpm
```

NoMachine is not installed from the rpm-ics repository because the nomachine (free) and nomachine-terminal-server RPMs conflict otherwise.
nomachine-terminal-server would always take precedence.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-nx-terminalsrv
```

License
-------

BSD 2-clause
